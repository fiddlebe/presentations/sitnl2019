# SITNL2019

This is the presentation and session material for a presentation I did For SAP Inside Tracks Netherlands 2019.
It's about how I try to make my day-to-day work easier, using various tools, code-snippets and sneaky tricks.